/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package champollion;

/**
 *
 * @author ipradel
 */
public class Salle {
    private String intitule;
    private int capacite;
    private boolean reserve;

    public Salle(String intitule, int capacite) {
        this.intitule = intitule;
        this.capacite = capacite;
        this.reserve=false;
    }

    public void setReserve(boolean reserve) {
        this.reserve = reserve;
    }

    public String getIntitule() {
        return intitule;
    }

    public int getCapacite() {
        return capacite;
    }

    public boolean isReserve() {
        return reserve;
    }
    
}
