/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package champollion;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author ipradel
 */
public class Enseignant extends Personne {
    private List<ServicePrevu> myEnseignements = new LinkedList<ServicePrevu>();

    public Enseignant(String nom, String email) {
        super(nom, email);
    }
    
    /**
     * Ajoute un enseignement au service prévu pour cet enseignant
     * @param ue l'UE concernée
     * @param volumeCM le volume d'heures de cours magitral 
     * @param volumeTD le volume d'heures de TD 
     * @param volumeTP le volume d'heures de TP 
     */
    public void ajouteEnseignement(UE ue, int volumeCM, int volumeTP, int volumeTD){
        // TODO: Implémenter cette méthode
        ServicePrevu enseignement = new ServicePrevu(ue, this, volumeCM, volumeTP, volumeTD);
        myEnseignements.add(enseignement);
        
    }
    
     /**
     * Ajoute une intervention au service prévu pour cet enseignant
     * @param e l'Intervention concernée
     */
    
    public void ajouteIntervention(Intervention e){
        // TODO: Implémenter cette méthode
        throw new UnsupportedOperationException("Pas encore implémenté");
    }
    /**
     * Calcule le nombre total d'heures planifiées pour cet enseignant
     * en "heures équivalent TD"
     * Pour le calcul :
     * 1 heure de cours magistral vaut 1,5 h "équivalent TD"
     * 1 heure de TD vaut 1h "équivalent TD"
     * 1 heure de TP vaut 0,75h "équivalent TD"
     * @return le nombre total d'heures "équivalent TD" planifiées pour cet enseignant
     **/
    public int heuresPlanifiées(){
        int vol = 0;
        for(ServicePrevu sp : myEnseignements){
            vol = (int) (sp.getVolumeCM()+sp.getVolumeTD()+sp.getVolumeTP());}
        return vol;
    }
    
    /**
     * Calcule si l'enseignant est en sous service
     * c'est-à-dire si l'enseignant à moins de 192 heures de cours prévues
     * @return true si l'enseignant est en sous service / false sinon
     **/
    
    public boolean enSousService(){
        if (heuresPrevues()<192){
            return true;
        }
        else {
            return false;
        }
    }
    
    /**
     * Calcule le nombre total d'heures prévues pour cet enseignant
     * en "heures équivalent TD"
     * Pour le calcul :
     * 1 heure de cours magistral vaut 1,5 h "équivalent TD"
     * 1 heure de TD vaut 1h "équivalent TD"
     * 1 heure de TP vaut 0,75h "équivalent TD"
     * @return le nombre total d'heures "équivalent TD" prévues pour cet enseignant
     **/
    public int heuresPrevues() {
       int vol = 0;
        for(ServicePrevu sp : myEnseignements){
            vol = (int) (sp.getVolumeCM()*1.5+sp.getVolumeTD()+sp.getVolumeTP()*0.75);   
        }
        return vol;
    }

    /**
     * Calcule le nombre total d'heures prévues pour cet enseignant dans l'UE spécifiée
     * en "heures équivalent TD"
     * Pour le calcul :
     * 1 heure de cours magistral vaut 1,5 h "équivalent TD"
     * 1 heure de TD vaut 1h "équivalent TD"
     * 1 heure de TP vaut 0,75h "équivalent TD"
     * @param ue l'UE concernée
     * @return le nombre total d'heures "équivalent TD" prévues pour cet enseignant
     **/
    public int heuresPrevuesPourUE(UE ue) {
        int equiTD = 0;
        for(ServicePrevu sp : myEnseignements){
            if (sp.getUe()== ue){
                equiTD=(int) (sp.getVolumeCM()*1.5+sp.getVolumeTD()+sp.getVolumeTP()*0.75);
            }
        }
        return equiTD;
    }

}
