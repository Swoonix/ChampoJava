/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package champollion;

/**
 *
 * @author ipradel
 */
public class ServicePrevu {

    private int volumeCM;
    private int volumeTD;
    private int volumeTP;
    private UE ue;
    private Enseignant prof;

    public ServicePrevu(UE ue, Enseignant enseignant, int CM, int TP, int TD) {
        if(null == ue || null == enseignant) throw new NullPointerException();
        this.ue = ue;
        this.prof = enseignant;
        this.volumeCM=CM;
        this.volumeTP=TP;
        this.volumeTD=TD;
    }

    public UE getUe() {
        return ue;
    }

    public int getVolumeCM() {
        return volumeCM;
    }

    public int getVolumeTD() {
        return volumeTD;
    }

    public int getVolumeTP() {
        return volumeTP;
    }

    public Enseignant getProf() {
        return prof;
    }
}
