/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package champollion;

import java.util.Date;

/**
 *
 * @author ipradel
 */
public class Intervention {
    private Date debut;
    private int duree;
    private boolean annulee;
    private TypeIntervention type;

    public Intervention(Date debut, int duree, TypeIntervention type) {
        this.debut = debut;
        this.duree = duree;
        this.type=type;
        this.annulee=false;
    }
    
    public void annuler(){
        this.annulee=true;
    }
    
}
